from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoItemForm, TodoListForm


# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_object": todo_list,
    }
    return render(request, "todos/todolist.html", context)


def todo_list_detail(request, id):
    detail_lists = TodoList.objects.get(id=id)
    context = {
        "detail_lists": detail_lists,
    }
    return render(request, "todos/tododetail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update_list)
        if form.is_valid():
            update_list.save()
            return redirect("todo_list_detail", id=update_list.id)
    else:
        form = TodoListForm(instance=update_list)
    context = {"form": form}
    return render(request, "todos/update.html", context)


def todo_list_detele(request, id):
    if request.method == "POST":
        delete_list = TodoList.objects.get(id=id)
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/itemupdate.html", context)
